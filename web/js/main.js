window.onload = function () {
    var app = new Vue({
        el: "#player",
        data: {
            //用户输入的搜索内容
            query: "",
            //得到的歌曲列表
            musicList: [],
            //在线音乐的地址
            musicUrl: "",
            //歌曲封面地址
            coverUrl: "",
            //热门评论
            hotComments:[],
            //mv的地址
            mvUrl: "",
            //是否播放MV
            isShow: false,
            // 动画播放状态
            isPlaying: false
        },
        methods: {
            //搜索音乐
            searchMusic: function () {
                //保存this
                var self = this;
                axios.get("https://autumnfish.cn/search?keywords=" + this.query).then(
                    function (response) {
                        self.musicList = response.data.result.songs;
                    },
                    function (err) {
                    }
                )
            },
            //播放音乐
            playMusic: function (musicId) {
                var self = this;
                //设置歌曲的URL
                axios.get("https://autumnfish.cn/song/url?id="+musicId).then(
                    function (resp) {
                        self.musicUrl = resp.data.data[0].url;
                    },
                    function (err) {}
                );
                //设置歌曲的封面
                axios.get("https://autumnfish.cn/song/detail?ids="+musicId).then(
                    function (resp) {
                        self.coverUrl = resp.data.songs[0].al.picUrl
                    },
                    function (err) {}
                );
                //获取热门评论
                axios.get("https://autumnfish.cn/comment/hot?type=0&id="+musicId).then(
                    function (resp) {
                        self.hotComments = resp.data.hotComments;
                    },
                    function (err) {}
                );
            },
            //播放mv
            playMv:function (id) {
                var self = this;
                axios.get("https://autumnfish.cn/mv/url?id="+id).then(
                    function (resp) {
                        self.mvUrl = resp.data.data.url;
                        self.isShow = true;
                    },
                    function (err) {}
                );
            },
            //关闭MV
            closeMV:function () {
                this.isShow = false;
                this.mvUrl = "";
            },
            play: function () {
                this.isPlaying = true;
            },
            pause: function (){
                this.isPlaying = false;
            }
        }
    })
}

